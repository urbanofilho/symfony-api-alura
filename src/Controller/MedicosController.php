<?php
namespace App\Controller;

use App\Entity\Medico;
use App\Helper\ExtratorDadosRequest;
use App\Helper\MedicoFactory;
use App\Repository\MedicosRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MedicosController extends BaseController
{

    public function __construct(
        MedicosRepository $repository, 
        EntityManagerInterface $entityManager,
        MedicoFactory $medicoFactory,
        ExtratorDadosRequest $estratorDadosRequest
        ) {
        parent::__construct($repository, $entityManager, $medicoFactory, $estratorDadosRequest);

    }


    /**
    * @route("/especialidades/{especialidadeId}/medicos", methods={"GET"})
     */
    public function buscarPorEspecialidade(int $especialidadeId): Response
    {

        $medicos = $this->repository->findBy([
            'especialidade' => $especialidadeId
        ]);
    
        //$codigoRetorno =  is_null($medicoExistente) ?  $codigoRetorno = Response::HTTP_NO_CONTENT : 200;
 
        return new JsonResponse($medicos);
    }

    /**
     * @param Medico $entidadeExistente
     * @param Medico $entidadeEnviada
     */
    public function atualizarEntidadeExistente($entidadeExistente, $entidadeEnviada){
        $entidadeExistente->setCrm($entidadeEnviada->getCrm());
        $entidadeExistente->setNome($entidadeEnviada->getNome());
        $entidadeExistente->setEspecialidade($entidadeEnviada->getEspecialidade());
    }
    

    
}