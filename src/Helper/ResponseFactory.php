<?php

namespace App\Helper;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ResponseFactory
{

    private $sucesso;
    private $paginaAtual;
    private $itensPorPagina;
    private $conteudoResposta;
    private $statusResposta;

    public function __construct(
       bool $sucesso,
       $conteudoResposta,
       $statusResposta = Response::HTTP_OK,
       int $paginaAtual = null,
       int $itensPorPagina = null
     ) {
        $this->sucesso = $sucesso;
        $this->paginaAtual = $paginaAtual;
        $this->itensPorPagina = $itensPorPagina;
        $this->conteudoResposta = $conteudoResposta;
        $this->statusResposta = $statusResposta;
     }

     public function getResponse(): JsonResponse
     {
         $conteudoResposta = [
             'sucesso' => $this->sucesso,
             'paginaAual' => $this->paginaAtual,
             'itensPorPagina' => $this->itensPorPagina,
             'conteudoReposta' => $this->conteudoResposta
         ];

         if(is_null($this->paginaAtual)){
            unset( $conteudoResposta['paginaAual']);
            unset( $conteudoResposta['itensPorPagina']);
         }

         return new JsonResponse($conteudoResposta, $this->statusResposta);
     }
}